<?php
require '../models/UserModel.php';

if (isset($_POST) && sizeof($_POST) > 0) {
    $user = new UserModel();
    $user->signup($_POST);
}
?>

<h2>Signup</h2>
<form action="signup.php" method="POST">
    <label>
        name: <input type="text" name="name">
    </label>
    <br>
    <label>
        Email: <input type="email" name="email">
    </label>
    <br>
    <label>
        Password: <input type="password" name="pwd">
    </label>
    <br>
    <label>
        Confirm Password: <input type="password" name="pwd2">
    </label>
    <br>
    <input type="submit" value="Signup">
</form>
