<?php
// namespace UserModel;

class UserModel
{
    public function __construct()
    {
        $this->connect();
    }
    protected $connection = null;

    public function connect()
    {
        $this->connection = new PDO("mysql:host=localhost;dbname=exercise", "root", "");
    }

    public function getAllUsers()
    {
        $query = $this->connection->prepare("SELECT * FROM users");
        $query->execute();

        return $query;
    }

    public function signup($user)
    {
        $name  = $user['name'];
        $email = $user['email'];
        $pwd   = $user['pwd'];
        if (empty($name) ||
            empty($email) ||
            empty($pwd) ||
            empty($user['pwd2'])) { //should check pwd
            throw new Exception("Signup failed. Information must not be empty!");
        } else {
            if (!preg_match("/^[a-zA-Z]*$/", $name)) {
                throw new Exception("Signup failed. Name invalid.");
            } else {
                if (!filter_var($user['email'], FILTER_VALIDATE_EMAIL)) {
                    throw new Exception("Signup failed. email invalid.");
                } else {
                    $hashed_password = password_hash($pwd, PASSWORD_DEFAULT);
                    $query           = $this->connection->prepare(
                        "INSERT INTO users (
                    name,
                    email,
                    pwd
                ) VALUES (
                    :name,
                    :email,
                    :pwd
                )"
                    );
                    $user = [
                        ':name' => $name,
                        ':email' => $email,
                        ':pwd' => $hashed_password,
                    ];
                    
                    $query->execute($user);
                    return true; //Signup succeeded
                }
            }
        }
    }
    public function signin($user)
    {
        if (empty($user['name']) ||
            empty($user['pwd'])) {
            header("Location: /error.php");
            exit();
        }
        $query = $this->connection->prepare(
            "INSERT INTO users (
            name,
            email,
            pwd
        ) VALUES (
            :name,
            :email,
            :pwd
        )"
        );

        $user = [
            ':name' => $user['name'],
            ':email' => $user['email'],
            ':pwd' => $user['pwd'],
        ];

        $query->execute($user);
        header("Location: /");
        exit();
    }
}
